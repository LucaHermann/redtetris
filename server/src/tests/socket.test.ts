import * as ioClient from "socket.io-client";
import { Room } from "../models/Room";
import { SocketEvent, GameSpeed, GameHardcore } from "../entities/SocketEvent";
import { socketManagement } from "../models/Socket";

// tslint:disable-next-line: no-var-requires
const app = require("express")();
// tslint:disable-next-line: no-var-requires
const server = require("http").Server(app);

// tslint:disable-next-line: no-var-requires
let ioServer = require("socket.io")(server, {
  cookie: false,
  pingTimeout: 30000,
  pingInterval: 2000,
});

const rooms: Room[] = [];

const socketUrl: string = "http://localhost:5000";
const options: any = {
  transports: ["websocket"],
  "force new connection": true,
};

const props = {
  roomPrivate: {
    room: {
      id: "",
      inGame: false,
      players: [],
      gameMaster: { id: "", userName: "", roomId: "" },
      isPrivate: false,
    },
    error: "The room is private",
  },
  roomNotExisting: {
    room: {
      id: "3",
      inGame: false,
      players: [],
      gameMaster: { id: "5", userName: "5", roomId: "3" },
      isPrivate: false,
    },
  },
  userState: {
    user1: {
      id: "1",
      userName: "1",
      roomId: "1",
    },
    user2: {
      id: "2",
      userName: "2",
      roomId: "1",
    },
    user3: {
      id: "3",
      userName: "3",
      roomId: "1",
    },
    user4: {
      id: "4",
      userName: "4",
      roomId: "1",
    },
    user5: {
      id: "5",
      userName: "5",
      roomId: "1",
    },
    roomSettings: {
      setMusic: true,
      setEffect: false,
      setSpectrum: false,
    },
  },
  roomState: {
    room: {
      id: "1",
      inGame: false,
      players: [
        {
          id: "1",
          userName: "1",
          roomId: "1",
        },
        {
          id: "2",
          userName: "2",
          roomId: "1",
        },
        {
          id: "3",
          userName: "3",
          roomId: "1",
        },
        {
          id: "4",
          userName: "4",
          roomId: "1",
        },
      ],
      gameMaster: {
        id: "1",
        userName: "1",
        roomId: "1",
      },
      isPrivate: false,
    },
    gameSettings: {
      gameSpeed: GameSpeed.NORMAL,
      rotations: true,
      hardcore: GameHardcore.ONE_LINE_FIVE_LIVES,
    },
    gameSettingsDone: false,
  },
};

describe("Server", () => {
  describe("Socket Events", () => {
    let client: SocketIOClient.Socket;

    beforeEach(() => {
      ioServer = socketManagement(ioServer, rooms);
      ioServer.listen(5000);
      client = ioClient.connect(socketUrl, options);
    });

    afterEach(() => {
      ioServer.close();
      client.close();
    });

    test("should test the socket connection", (done: any) => {
      ioServer.on(SocketEvent.CONNECT, (socket: any) => {
        if (socket) {
          done();
        }
      });
    });

    test("should register event \"CreatePlayerId\"", (done: any) => {
      client.on(SocketEvent.CONNECT, () => {
        client.on(SocketEvent.CREATE_USER, (data: string) => {
          if (data) {
            done();
          }
        });
      });
    });
    describe("testing joining room different cases", () => {
      test("Should Join room and create one", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.JOIN_ROOM, { rest: props.userState.user1 });
          client.on(SocketEvent.JOINING_ROOM, (data: any) => {
            if (data.room && data.error === "" && data.gameSettings) {
              done();
            }
          });
        });
      });

      test("Should Join room but already exists and is private", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.JOIN_ROOM, { rest: props.userState.user2 });
          client.on(SocketEvent.JOINING_ROOM, (data: any) => {
            if (data.error === props.roomPrivate.error && data.room.id === props.roomPrivate.room.id) {
              done();
            }
          });
        });
      });
      describe("Testing GameSettings by setting isPrivate to false to test the room full case", () => {
        test("Should update game settings of the room", (done: any) => {
          client.on(SocketEvent.CONNECT, () => {
            client.emit(SocketEvent.REFRESH_GAME_SETTINGS, {
              rest: { gameSettings: props.roomState.gameSettings, room: props.roomState.room },
            });
            client.on(SocketEvent.REFRESH_ROOM, (data: any) => {
              if (
                data.gameSettings.gameSpeed === props.roomState.gameSettings.gameSpeed &&
                data.gameSettings.rotations === props.roomState.gameSettings.rotations &&
                data.gameSettings.hardcore === props.roomState.gameSettings.hardcore &&
                data.room.isPrivate === false
              ) {
                // rooms[0].room = data.room;
                // rooms[0].gameSettings = data.gameSettings;
                done();
              }
            });
          });
        });
        test("Should not update game settings of the room because it does not exists", (done: any) => {
          client.on(SocketEvent.CONNECT, () => {
            client.emit(SocketEvent.REFRESH_GAME_SETTINGS, {
              rest: { gameSettings: props.roomState.gameSettings, room: props.roomNotExisting },
            });
            done();
          });
        });
      });
      test("Should join Room with player2-3-4", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.JOIN_ROOM, { rest: props.userState.user2 });
          client.emit(SocketEvent.JOIN_ROOM, { rest: props.userState.user3 });
          client.emit(SocketEvent.JOIN_ROOM, { rest: props.userState.user4 });
          client.on(SocketEvent.JOINING_ROOM, (data: any) => {
            if (data.room.players.length === 4) {
              done();
            }
          });
        });
      });
      test("Should join Room with player5 but returns the full error", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.JOIN_ROOM, { rest: props.userState.user5 });
          client.on(SocketEvent.JOINING_ROOM, (data: any) => {
            if (data.error === "The room is full") {
              done();
            }
          });
        });
      });
      test("Should join Room with player5 but returns the full error", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.JOIN_ROOM, { rest: props.userState.user5 });
          client.on(SocketEvent.JOINING_ROOM, (data: any) => {
            if (data.error === "The room is full") {
              done();
            }
          });
        });
      });
    });
    describe("Testing leave room action", () => {
      test("Should leave room for user1", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.LEAVE_ROOM, { rest: props.userState.user1 });
          client.on(SocketEvent.REFRESH_ROOM, (data: any) => {
            if (rooms[0].room.gameMaster.id === "2") {
              done();
            }
          });
        });
      });
      test("Should leave room for user2", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.LEAVE_ROOM, { rest: props.userState.user2 });
          client.on(SocketEvent.REFRESH_ROOM, (data: any) => {
            if (rooms[0].room.gameMaster.id === "3") {
              done();
            }
          });
        });
      });
      test("Should leave room for user3", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.LEAVE_ROOM, { rest: props.userState.user3 });
          client.on(SocketEvent.REFRESH_ROOM, (data: any) => {
            if (rooms[0].room.gameMaster.id === "4") {
              done();
            }
          });
        });
      });
      test("Should leave room for user4", (done: any) => {
        client.on(SocketEvent.CONNECT, () => {
          client.emit(SocketEvent.LEAVE_ROOM, { rest: props.userState.user4 });
          client.on(SocketEvent.REFRESH_ROOM, (data: any) => {
            if (!rooms[0]) {
              done();
            }
          });
        });
      });
    });
  });
});
