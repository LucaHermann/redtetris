import { pieces } from "./Pieces";

interface IPos {
  x: number;
  y: number;
}
export class Piece {
  public shape: Array<Array<number>>;
  public color: string;
  // public width: number;
  // public height: number;
  public pos: IPos;
  public tetrimino: string;

  constructor(tetrimino: string, pos = { x: 4, y: 0 }) {
    this.tetrimino = tetrimino;
    const piece: any = pieces.find((p) => p.name === tetrimino);
    this.shape = piece.shape;
    this.color = piece.color;
    // this.width = this.shape[0].length - 1;
    // this.height = this.shape.length - 1;
    this.pos = pos;
  }

  public currentPieceRotation(p: Piece) {
    // Clone with JSON for immutability.
    const clone: Piece = JSON.parse(JSON.stringify(p));
    // Transpose matrix, p is the Piece.
    for (let y = 0; y < p.shape.length; ++y) {
      for (let x = 0; x < y; ++x) {
        [p.shape[x][y], p.shape[y][x]] =
          [p.shape[y][x], p.shape[x][y]];
      }
    }
    // Reverse the order of the columns.
    p.shape.forEach(row => row.reverse());
    return clone;
  }
}