export class Board {
  public grid: number[][];

  constructor(tetrimino: string) {
    this.grid = Array.from({ length: 10 }, () => Array(20).fill(0));
  }
}