import { IUser, IRoom, IGameSettings, GameSpeed, GameHardcore } from "./SocketEvent";
import * as _ from "lodash";

export class Room {
  public room: IRoom;
  public gameSettings: IGameSettings;
  constructor(user: IUser) {
    this.room = this.createRoom(user);
    this.gameSettings = {
      gameSpeed: GameSpeed.NORMAL,
      rotations: false,
      hardcore: GameHardcore.NONE,
    };
  }

  public createRoom(user: IUser): IRoom {
    const room: IRoom = {
      id: user.roomId,
      inGame: false,
      gameMaster: user,
      players: [user],
      isPrivate: true,
    };
    return room;
  }

  public addPlayer(user: IUser): void {
    this.room.players.push(user);
  }

  public isRoomPublic(): boolean {
    return this.room.isPrivate;
  }

  public isRoomFull(): boolean {
    return this.room.players.length > 3 ? true : false;
  }

  public setRoomPrivate(isPrivate: boolean): void {
    this.room.isPrivate = isPrivate;
  }

  public removePlayer(user: IUser): void {
    _.remove(this.room.players, user);
    // tslint:disable-next-line: no-unused-expression
    this.room.players.length < 1 ? null : (this.room.gameMaster = this.room.players[0]);
  }

  public setRotations(rotations: boolean): void {
    this.gameSettings.rotations = rotations;
  }

  public setHardcore(hardcore: GameHardcore): void {
    this.gameSettings.hardcore = hardcore;
  }

  public setGameSpeed(gameSpeed: GameSpeed): void {
    this.gameSettings.gameSpeed = gameSpeed;
  }
}
