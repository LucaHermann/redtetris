import React from "react";
import { configure, mount } from "enzyme";
import * as ReactSixteenAdapter from "enzyme-adapter-react-16";
import SettingCheckBox from "../components/SettingCheckBox";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
// import { Provider } from 'react-redux';

const adapter = ReactSixteenAdapter as any;
configure({ adapter: new adapter.default() });

const props = {
  userState: {
    roomSettings:
    {
      setMusic: false,
      setEffect: false,
      setSpectrum: false,
    }
  },
  roomState: {
    room: {
      id: "1",
      inGame: false,
      players: [
        {
          id: "1",
          userName: "1",
          roomId: "1"
        },
        {
          id: "2",
          userName: "2",
          roomId: "1"
        },
      ],
      gameMaster: {
        id: "1",
        userName: "1",
        roomId: "1"
      },
      isPrivate: false
    }
  },
  refreshSettings: jest.fn()
};

const mockStore = configureStore([thunk]);
// TODO CONFIGURE STATE OF STORE APPARENTLY
let store;
describe("<SettingCheckBox />", () => {
  store = mockStore(props);
  const wrapper = mount(<Provider store={store}><SettingCheckBox /></Provider>);

  it("Set musicSetting to true and change label, testing useState and handleChanges", () => {
    wrapper.find("Checkbox[name=\"musicSetting\"]").simulate("change", { target: { checked: true } });
    expect(wrapper.find("Checkbox[name=\"musicSetting\"]").prop("label")).toEqual("Music is on");
  });

  it("Set musicSetting to true and change label, testing useState and handleChanges", () => {
    store = mockStore(props);
    wrapper.find("Checkbox[name=\"effectSetting\"]").simulate("change", { target: { checked: true } });
    expect(wrapper.find("Checkbox[name=\"effectSetting\"]").prop("label")).toEqual("Effects are enabled");
  });

  it("Set musicSetting to true and change label, testing useState and handleChanges", () => {
    store = mockStore(props);
    wrapper.find("Checkbox[name=\"spectrumSetting\"]").simulate("change", { target: { checked: true } });
    expect(wrapper.find("Checkbox[name=\"spectrumSetting\"]").prop("label")).toEqual("Spectrums are enabled");
  });
});
