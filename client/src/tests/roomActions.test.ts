import thunk, { ThunkMiddleware } from "redux-thunk";
import configureMockStore from "redux-mock-store";
import { socketMiddleware } from "../middlewares/socketMiddleware";
import * as RoomActions from "../actions/roomActions";
import * as UserActions from "../actions/userActions";
import { GameSpeed, GameHardcore } from "../reducers/roomReducer";
import { IAppState } from "../store/Store";

import express from "express";
import socketIo from "socket.io";
import { createServer, Server } from "http";
import { Room } from "mocks/mockServer/Room";

const middleware = [thunk as ThunkMiddleware<IAppState, RoomActions.RoomActions>, socketMiddleware()];
const mockStore = configureMockStore(middleware);
// tslint:disable-next-line: no-var-requires
import { socketManagement } from "../../mocks/mockServer/Socket";

/*
 ********************* MOCK SERVER *********************
 */

let _app: express.Application;
let server: Server;
let port: number;
let io: SocketIO.Server;
let rooms: Room[];

function listen(): void {
  server.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
    socketManagement(io, rooms);
  });
}
port = 8080;
_app = express();
server = createServer(_app);
io = socketIo(server);
rooms = [];
listen();

const playerTest = {
  id: "1",
  userName: "1",
  roomId: "1",
};

const roomTest = {
  room: {
    id: "1",
    inGame: false,
    players: [
      {
        id: "1",
        userName: "1",
        roomId: "1",
      },
    ],
    gameMaster: {
      id: "1",
      userName: "1",
      roomId: "1",
    },
    isPrivate: true,
  },
  error: "",
  gameSettings: {
    gameSpeed: "NORMAL",
    rotations: false,
    hardcore: "NONE",
  },
  gameSettingsDone: false,
};

const settings = {
  room: {
    id: "1",
    inGame: false,
    players: [
      {
        id: "1",
        userName: "1",
        roomId: "1",
      },
    ],
    gameMaster: {
      id: "1",
      userName: "1",
      roomId: "1",
    },
    isPrivate: false,
  },
  gameSettings: {
    gameSpeed: GameSpeed.NORMAL,
    rotations: false,
    hardcore: GameHardcore.NONE,
  },
};

const mockState = {
  room: roomTest,
};

describe("Actions", () => {
  afterAll(() => {
    io.close();
  });

  it("Should create an action JoiningRoom", async () => {
    const store = mockStore({});
    store.getState = () => mockState;
    const expectedResponse = [
      {
        type: RoomActions.RoomActionType.JOINING_ROOM,
        room: roomTest.room,
        error: roomTest.error,
        gameSettings: roomTest.gameSettings,
      },
    ];
    store.dispatch(UserActions.JoinRoom(playerTest));
    return store.dispatch<any>(RoomActions.JoiningRoom()).then(() => {
      expect(store.getActions()).toEqual(expectedResponse);
    });
  });

  it("Should create an action REFRESH_GAME_SETTINGS", async () => {
    const expectedResponse2 = {
      type: RoomActions.RoomActionType.REFRESH_GAME_SETTINGS,
      event: RoomActions.RoomActionType.REFRESH_GAME_SETTINGS,
      emit: true,
      rest: settings,
    };
    expect(RoomActions.RefreshGameSettings(settings)).toEqual(expectedResponse2);
  });

  it("Should create an action to refresh game settings done", async () => {
    const store = mockStore({});
    const expectedResponse = [
      {
        type: RoomActions.RoomActionType.GAME_SETTINGS_DONE,
        gameSettingsDone: true,
      },
    ];
    return store.dispatch<any>(RoomActions.GameSettingsDone(true)).then(() => {
      expect(store.getActions()).toEqual(expectedResponse);
    });
  });

  it("Should create an action to REFRESH_ROOM", async () => {
    const store = mockStore({});
    store.getState = () => mockState;
    const expectedResponse = [
      {
        type: RoomActions.RoomActionType.REFRESH_ROOM,
        room: roomTest.room,
        error: roomTest.error,
        gameSettings: roomTest.gameSettings,
      },
    ];
    expectedResponse[0].room.isPrivate = false;
    store.dispatch(RoomActions.RefreshGameSettings(settings));
    return store.dispatch<any>(RoomActions.RefreshRoom()).then(() => {
      expect(store.getActions()).toEqual(expectedResponse);
    });
  });
});
