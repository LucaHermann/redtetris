import React from "react";
import { configure, mount } from "enzyme";
import * as ReactSixteenAdapter from "enzyme-adapter-react-16";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { GameSpeed, GameHardcore } from "../reducers/roomReducer";
import FinalRoom from "../containers/FinalRoom";
import { createMemoryHistory, createLocation } from "history";
import { match } from "react-router";
// import { RefreshRoomSettings } from '../actions/userActions';

// import { Provider } from 'react-redux';

const adapter = ReactSixteenAdapter as any;
configure({ adapter: new adapter.default() });

const props = {
    userState: {
        user: {
            id: "1",
            userName: "1",
            roomId: "1"
        },
        roomSettings:
        {
            setMusic: true,
            setEffect: false,
            setSpectrum: false,
        }
    },
    roomState : {
        room: {
            id: "1",
            inGame: false,
            players: [
                {
                    id: "1",
                    userName: "1",
                    roomId: "1"
                },
                {
                    id: "2",
                    userName: "2",
                    roomId: "1"
                },
            ],
            gameMaster: {
                id: "1",
                userName: "1",
                roomId: "1"
            },
            isPrivate: false
        },
    gameSettings: {
      gameSpeed: GameSpeed.NORMAL,
      rotations: true,
      hardcore: GameHardcore.ONE_LINE_FIVE_LIVES,
    },
    gameSettingsDone: false,
  },
  refreshRoom: jest.fn(),
  leaveRoom: jest.fn(),
  updateUser: jest.fn(),
  refreshGameSettings: jest.fn(),
  updateGameSettingsDone: jest.fn(),
};

const history = createMemoryHistory();
const path = `/1[1]`;

const match: match<{ id: string }> = {
    isExact: false,
    path,
    url: path.replace(":id", "1"),
    params: { id: "1" }
};

const mockStore = configureMockStore([thunk]);
// TODO CONFIGURE STATE OF STORE APPARENTLY
describe("<FinalRoomPage />", () => {
    const location = createLocation(match.url);

    let store = mockStore(props);

    describe("Without gameSettings done, not rendering roomPage", () => {
        const wrapper = mount(
            <Provider store={store}><FinalRoom history={history} match={match} location={location} /></Provider>
        );
        expect(wrapper.find("RoomPage").length).toEqual(0);
        expect(wrapper.find("GameSettings").length).toEqual(1);
    });

    describe("With gameSettingsDone not rendering GameSettings", () => {
        props.roomState.gameSettingsDone = true;

        it("Should not render gameSetting page", () => {
            const wrapper = mount(
                <Provider store={store}><FinalRoom history={history} match={match} location={location} /></Provider>
            );
            expect(wrapper.find("RoomPage").length).toEqual(1);
            expect(wrapper.find("GameSettings").length).toEqual(0);
        });

        it("trigger handleMusic when mounted with music on", async () => {
            store = mockStore(props);
            const wrapper = mount(
                <Provider store={store}><FinalRoom history={history} match={match} location={location} /></Provider>
            );
            expect(wrapper.find("Checkbox[name=\"musicSetting\"]").prop("label")).toEqual("Music is on");
        });

        it("trigger handleMusic when mounted with music on", async () => {
            props.userState.roomSettings.setMusic = false;
            store = mockStore(props);
            const wrapper = mount(
                <Provider store={store}><FinalRoom history={history} match={match} location={location} /></Provider>
            );
            expect(wrapper.find("Checkbox[name=\"musicSetting\"]").prop("label")).toEqual("Music is off");
        });
    });

    describe("mount without room id but in roompage", () => {
        props.userState.user.roomId = "";
        it("should redirect to '/'", () => {
            store = mockStore(props);
            const wrapper = mount(
                <Provider store={store}><FinalRoom history={history} match={match} location={location} /></Provider>
            );
            wrapper.update();
            expect(history.action).toEqual("PUSH");
            expect(history.location.pathname).toEqual("/");
        });
    });
});
