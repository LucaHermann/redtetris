import { userReducer } from "../reducers/userReducers";
import * as userActions from "../actions/userActions";

describe("userReducer", () => {
  const initialState = {
    user: {
      id: "",
      userName: "",
      roomId: "",
    },
    roomSettings: {
      setMusic: false,
      setEffect: false,
      setSpectrum: false,
    },
  };

  const playerTest = {
    id: "testId",
    userName: "testName",
    roomId: "roomTest",
  };

  const createPlayerTest = {
    id: "testId",
    userName: "",
    roomId: "",
  };

  const newRoomSettings = {
    setMusic: true,
    setEffect: false,
    setSpectrum: true,
  };

  it("Should test the id after we created the player", async () => {
    expect(
      userReducer(initialState, {
        type: userActions.UserActionType.CREATE_USER,
        user: createPlayerTest,
      }),
    ).toEqual({
      ...initialState,
      user: createPlayerTest,
    });
  });

  it("Should test the user after we updated it", async () => {
    expect(
      userReducer(initialState, {
        type: userActions.UserActionType.UPDATE_USER,
        user: playerTest,
      }),
    ).toEqual({
      ...initialState,
      user: playerTest,
    });
  });

  it("Should test the room settings after they changed", async () => {
    expect(
      userReducer(initialState, {
        type: userActions.UserActionType.REFRESH_ROOM_SETTINGS,
        roomSettings: newRoomSettings,
      }),
    ).toEqual({
      ...initialState,
      roomSettings: newRoomSettings,
    });
  });

  it("should test the DEFAULT reducer", () => {
    expect(
      userReducer(initialState, {
        type: userActions.UserActionType.DEFAULT,
      }),
    ).toEqual(initialState);
  });
});
