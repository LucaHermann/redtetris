import thunk, { ThunkMiddleware } from "redux-thunk";
import configureMockStore from "redux-mock-store";
import { socketMiddleware } from "../middlewares/socketMiddleware";
import * as actions from "../actions/userActions";
import { IAppState } from "../store/Store";
import { Room } from "../../mocks/mockServer/Room";

import express from "express";
import socketIo from "socket.io";
import { createServer, Server } from "http";
import { socketManagement } from "../../mocks/mockServer/Socket";

const middlewares = [thunk as ThunkMiddleware<IAppState, actions.UserActions>, socketMiddleware()];
const mockStore = configureMockStore(middlewares);

const playerTest = {
  id: "PlayerTestId",
  userName: "playerTest",
  roomId: "rooms234523sfdTaest",
};

const roomSettings = {
  setMusic: false,
  setEffect: false,
  setSpectrum: false,
};

const mockState = {
  user: playerTest,
  roomSettings,
};

/*
********************* MOCK SERVER *********************
*/
// tslint:disable-next-line: no-var-requires

let _app: express.Application;
let server: Server;
let port: number;
let io: SocketIO.Server;
let rooms: Room[];

function listen(): void {
  server.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
    socketManagement(io, rooms);
  });
}
port = 8080;
_app = express();
server = createServer(_app);
io = socketIo(server);
rooms = [];
listen();

describe("Actions", () => {
  afterAll(() => {
    io.close();
  });

  afterAll(() => {
    io.close();
  });

  it("Should create an action to CREATE_USER", async () => {
    const store = mockStore({});
    store.getState = () => mockState;
    const expectedResponse = [
      {
        type: actions.UserActionType.CREATE_USER,
        user: {
          id: "",
          roomId: "",
          userName: "",
        },
      },
    ];
    return store.dispatch<any>(actions.CreateUser()).then(() => {
      store.getState = () => {
        const test1 = store.getActions();
        expectedResponse[0].user.id = test1[0].user.id;
        playerTest.id = test1[0].user.id;
      };
      store.getState();
      expect(store.getActions()).toEqual(expectedResponse);
    });
  });

  it("Should create and action to JOIN_ROOM", async () => {
    const expectedResponse = {
      type: actions.UserActionType.JOIN_ROOM,
      event: actions.UserActionType.JOIN_ROOM,
      emit: true,
      rest: playerTest,
    };
    expect(actions.JoinRoom(playerTest)).toEqual(expectedResponse);
  });

  it("Should create an action to UPDATE_USER", async () => {
    const store = mockStore({});
    store.getState = () => mockState;
    const expectedResponse = [
      {
        type: actions.UserActionType.UPDATE_USER,
        user: playerTest,
      },
    ];
    store.dispatch(actions.JoinRoom(playerTest));
    return store.dispatch<any>(actions.UpdateUser()).then(() => {
      expect(store.getActions()).toEqual(expectedResponse);
    });
  });

  it("Should create an action to LEAVE_ROOM", async () => {
    const expectedResponse = {
      type: actions.UserActionType.LEAVE_ROOM,
      event: actions.UserActionType.LEAVE_ROOM,
      emit: true,
      rest: playerTest,
    };
    expect(actions.LeaveRoom(playerTest)).toEqual(expectedResponse);
  });

  it("Should create an action to REFRESH_ROOM_SETTINGS", async () => {
    const store = mockStore({});
    const expectedResponse = [
      {
        type: actions.UserActionType.REFRESH_ROOM_SETTINGS,
        roomSettings
      }
    ];
    return store.dispatch<any>(actions.RefreshRoomSettings(roomSettings)).then(() => {
      expect(store.getActions()).toEqual(expectedResponse);
    });
  });
});
