import React, { useState, useEffect } from "react";
import { Checkbox, List, Segment } from "semantic-ui-react";
import { connect } from "react-redux";
import { cssRaw } from "typestyle";

import { IAppState } from "../store/Store";
import { IRoom, IRoomSettings } from "../reducers/roomReducer";
import * as userActions from "../actions/userActions";

cssRaw(`
  .ListItems {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
`);

interface IProps {
  room: IRoom;
  roomSettings: IRoomSettings;
  refreshSettings: (settings: IRoomSettings) => void;
}

export const SettingCheckBox: React.FC<IProps> = props => {
  const { refreshSettings, roomSettings } = props;
  const [sliders, setSliders] = useState({
    setMusic: roomSettings.setMusic,
    setEffect: roomSettings.setEffect,
    setSpectrum: roomSettings.setSpectrum,
  });

  useEffect(() => {
    refreshSettings(sliders);
  }, [sliders]);

  const MusicChanged = (e: React.ChangeEvent<HTMLInputElement>, { checked }: any): void => {
    e.preventDefault();
    setSliders({ setMusic: checked, setEffect: sliders.setEffect, setSpectrum: sliders.setSpectrum });
  };

  const EffectChanged = (e: React.ChangeEvent<HTMLInputElement>, { checked }: any): void => {
    e.preventDefault();
    setSliders({ setMusic: sliders.setMusic, setEffect: checked, setSpectrum: sliders.setSpectrum });
  };

  const SpectrumChanged = (e: React.ChangeEvent<HTMLInputElement>, { checked }: any): void => {
    e.preventDefault();
    setSliders({ setMusic: sliders.setMusic, setEffect: sliders.setEffect, setSpectrum: checked });
  };

  return (
    <Segment stacked>
      <List className="ListItems">
        <List.Item>
          <List.Content>
            <Checkbox
              className="checkbox"
              name="musicSetting"
              slider
              label={!sliders.setMusic ? "Music is off" : "Music is on"}
              onChange={MusicChanged}
              checked={sliders.setMusic}
            />
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content>
            <Checkbox
              className="checkbox"
              name="effectSetting"
              slider
              label={!sliders.setEffect ? "Effects are disabled" : "Effects are enabled"}
              checked={sliders.setEffect}
              onChange={EffectChanged}
            />
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content>
            <Checkbox
              className="checkbox"
              name="spectrumSetting"
              slider
              label={!sliders.setSpectrum ? "Spectrums are disabled" : "Spectrums are enabled"}
              checked={sliders.setSpectrum}
              onChange={SpectrumChanged}
            />
          </List.Content>
        </List.Item>
      </List>
    </Segment>
  );
};

const mapStateToProps = (store: IAppState) => {
  return {
    room: store.roomState.room,
    roomSettings: store.userState.roomSettings,
  };
};

const mapDispatchToProps = {
  refreshSettings: (settings: IRoomSettings) => userActions.RefreshRoomSettings(settings),
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingCheckBox);
