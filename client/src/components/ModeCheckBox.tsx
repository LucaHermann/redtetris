import React, { useState } from "react";
import { Header, Segment, Checkbox } from "semantic-ui-react";

import { IRoom, IGameSettings } from "../reducers/roomReducer";

interface IProps {
  room: IRoom;
  callBackHandler: any;
  gameSettings: IGameSettings;
}

const ModeCheckBox: React.SFC<IProps> = props => {
  const [form, setForm] = useState({
    mode: "Solo",
  });

  const modeChanged = (e: React.ChangeEvent<HTMLInputElement>, { value }: any) => {
    e.preventDefault();
    setForm({ mode: value });
    props.room.isPrivate = value === "Solo" ? true : false;
    props.callBackHandler({ room: props.room, gameSettings: props.gameSettings });
  };

  return (
    <div className="ModeCheckBox">
      <Header as="h2" style={{ color: "#31E981" }} textAlign="center">
        Choose Mode:
      </Header>
      <div>
        <Segment stacked>
          <Checkbox
            className="Checkbox"
            slider
            value="Solo"
            label="Solo"
            name="Solo"
            checked={form.mode === "Solo"}
            onChange={modeChanged}
          />
          <Checkbox
            className="Checkbox"
            slider
            value="Multiplayer"
            label="Multiplayer"
            name="Multiplayer"
            onChange={modeChanged}
            checked={form.mode === "Multiplayer"}
          />
        </Segment>
      </div>
    </div>
  );
};

export default ModeCheckBox;
