import { ActionCreator, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";

import { IRoomSettings } from "../reducers/roomReducer";
import { IUser, IUserState } from "../reducers/userReducers";

export enum UserActionType {
  JOIN_ROOM = "join_room",
  CREATE_USER = "create_user",
  UPDATE_USER = "update_user",
  LEAVE_ROOM = "leave_room",
  REFRESH_ROOM_SETTINGS = "refresh_room_settings",
  DEFAULT = "DEFAULT",
}

export interface CreateUserActionType {
  type: UserActionType.CREATE_USER;
  user: IUser;
}

export interface UpdateUserStateActionType {
  type: UserActionType.UPDATE_USER;
  user: IUser;
}

export interface UserJoinRoomActionType {
  type: UserActionType.JOIN_ROOM;
  user: IUser;
}

export interface UpdateUserActionType {
  type: UserActionType.UPDATE_USER;
  user: IUser;
}

export interface SettingsRoomTypeNested {
  roomSettings: IRoomSettings;
}

export interface RefreshRoomSettingsType {
  type: UserActionType.REFRESH_ROOM_SETTINGS;
  roomSettings: IRoomSettings;
}

export interface UserActionDefaultType {
  type: UserActionType.DEFAULT;
}

export type UserActions =
  | CreateUserActionType
  | UpdateUserStateActionType
  | RefreshRoomSettingsType
  | UserActionDefaultType;

export const CreateUser: ActionCreator<ThunkAction<Promise<any>, IUserState, null, CreateUserActionType>> = () => {
  return async (dispatch: Dispatch) => {
    return new Promise<any>(resolve => {
      dispatch({
        event: UserActionType.CREATE_USER,
        type: UserActionType.CREATE_USER,
        handle: (id: string) => {
          resolve(
            dispatch({
              type: UserActionType.CREATE_USER,
              user: {
                id,
                userName: "",
                roomId: "",
              },
            }),
          );
        },
      });
    });
  };
};

export const JoinRoom = (user: IUser) => {
  return {
    event: UserActionType.JOIN_ROOM,
    type: UserActionType.JOIN_ROOM,
    emit: true,
    rest: user,
  };
};

export const UpdateUser: ActionCreator<ThunkAction<Promise<any>, IUserState, null, UserActions>> = () => {
  return async (dispatch: Dispatch) => {
    return new Promise<any>(resolve => {
      dispatch({
        event: UserActionType.UPDATE_USER,
        type: UserActionType.UPDATE_USER,
        handle: (user: IUser) => {
          resolve(
            dispatch({
              type: UserActionType.UPDATE_USER,
              user,
            }),
          );
        },
      });
    });
  };
};

export const LeaveRoom = (user: IUser) => {
  return {
    event: UserActionType.LEAVE_ROOM,
    type: UserActionType.LEAVE_ROOM,
    emit: true,
    rest: user,
  };
};

export const RefreshRoomSettings: ActionCreator<ThunkAction<
  Promise<any>,
  IUserState,
  SettingsRoomTypeNested,
  RefreshRoomSettingsType
>> = (settings: IRoomSettings) => {
  return async (dispatch: Dispatch) => {
    return new Promise<any>(resolve => {
      resolve(
        dispatch({
          type: UserActionType.REFRESH_ROOM_SETTINGS,
          roomSettings: settings,
        }),
      );
    });
  };
};
