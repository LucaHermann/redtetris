import { Reducer } from "redux";

import { UserActions, UserActionType } from "../actions/userActions";
import { IRoomSettings } from "./roomReducer";

export interface IUser {
  id: string;
  userName?: string;
  roomId?: string;
}

export interface IUserState {
  readonly user: IUser;
  readonly roomSettings?: IRoomSettings;
}

const initialUserState: IUserState = {
  user: {
    id: "",
    userName: "",
    roomId: "",
  },
  roomSettings: {
    setMusic: false,
    setEffect: false,
    setSpectrum: false,
  },
};

export const userReducer: Reducer<IUserState, UserActions> = (state = initialUserState, action) => {
  switch (action.type) {
    case UserActionType.CREATE_USER:
      return {
        ...state,
        user: action.user,
      };
    case UserActionType.UPDATE_USER:
      return {
        ...state,
        user: action.user,
      };
    case UserActionType.REFRESH_ROOM_SETTINGS:
      return {
        ...state,
        roomSettings: action.roomSettings,
      };
    default:
      return state;
  }
};
