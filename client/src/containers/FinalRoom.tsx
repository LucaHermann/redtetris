import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";

import { IAppState } from "../store/Store";
import { IUser } from "../reducers/userReducers";
import { IRoom, IRoomSettings, IGameSettings } from "../reducers/roomReducer";
import * as roomAction from "../actions/roomActions";
import * as userAction from "../actions/userActions";
import RoomPage from "./RoomPage";
import GameSettings from "./GameSettings";
import GamePage from "./GamePage";

interface Props extends RouteComponentProps {
  user: IUser;
  room: IRoom;
  roomSettings: IRoomSettings;
  gameSettings: IGameSettings;
  refreshRoom: () => void;
  updateUser: () => void;
  leaveRoom: (user: IUser) => void;
  gameSettingsDone: boolean;
  refreshGameSettings: ({ room: IRoom, gameSettings: IGameSettings }: roomAction.SettingsGameTypeNested) => void;
  updateGameSettingsDone: (done: boolean) => void;
}

const FinalRoomPage: React.FC<Props> = props => {
  const url = "https://ia600504.us.archive.org/33/items/TetrisThemeMusic/Tetris.mp3";
  const [volume, setVolume] = useState({
    audio: new Audio(url),
    on: false,
  });

  const { refreshRoom, updateUser, leaveRoom, updateGameSettingsDone } = props;
  useEffect(() => {
    refreshRoom();
  }, [refreshRoom, props.room]);
  useEffect(() => {
    updateUser();
  }, [props.user]);
  useEffect(() => {
    updateGameSettingsDone(false);
  }, [leaveRoom]);

  useEffect(() => {
    handleMusic();
  }, [props.roomSettings.setMusic]);

  const handleMusic = () => {
    if (props.roomSettings.setMusic === false) {
      setVolume({ ...volume, on: false });
      volume.audio.pause();
    } else {
      setVolume({ ...volume, on: true });
      volume.audio.loop = true;
      volume.audio.play();
    }
  };

  if (!props.user.roomId && props.user.userName) {
    props.history.push("/");
  }

  if (props.gameSettingsDone === false && props.user.id === props.room.gameMaster.id) {
    return <GameSettings />;
  } else if (props.gameSettingsDone === true && props.room.inGame === false) {
    return <RoomPage />;
  } else {
    return <GamePage />;
  }
};

const mapStateToProps = (store: IAppState) => {
  return {
    user: store.userState.user,
    room: store.roomState.room,
    roomSettings: store.userState.roomSettings,
    gameSettings: store.roomState.gameSettings,
    gameSettingsDone: store.roomState.gameSettingsDone,
  };
};

const mapDispatchToProps = {
  refreshRoom: () => roomAction.RefreshRoom(),
  leaveRoom: (user: IUser) => userAction.LeaveRoom(user),
  updateUser: () => userAction.UpdateUser(),
  refreshGameSettings: ({ room, gameSettings }: roomAction.SettingsGameTypeNested) =>
    roomAction.RefreshGameSettings({ room, gameSettings }),
  updateGameSettingsDone: (done: boolean) => roomAction.GameSettingsDone(done),
};

export default connect(mapStateToProps, mapDispatchToProps)(FinalRoomPage);
