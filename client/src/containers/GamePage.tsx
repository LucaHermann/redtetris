import React from "react";
import { connect } from "react-redux";
import { Container, Button, Header } from "semantic-ui-react";
import { cssRaw } from "typestyle";

import { IAppState } from "../store/Store";
import { IUser } from "../reducers/userReducers";
import { IRoom, IRoomSettings } from "../reducers/roomReducer";
import * as userAction from "../actions/userActions";
// import * as roomAction from "../actions/roomActions";
import CharacterList from "../components/UsersList";
import SettingsCheckBox from "../components/SettingCheckBox";
import Tetrominos from "./tetrominos.png";

cssRaw(`
  .row {
    display: flex;
    justify-content: space-between
  }

  @media (max-width: 600px) {
    .row {
      -webkit-flex-direction: column;
      flex-direction: column;
    }
  }
`);

interface Props {
  user: IUser;
  room: IRoom;
  roomSettings: IRoomSettings;
  leaveRoom: (user: IUser) => void;
  // setRoomInGame: (room: IRoom) => void;
}

const GamePage: React.FC<Props> = props => {
  const { room, leaveRoom } = props;

  return (
    <Container>
      <img src={Tetrominos} className="Tetrominos" alt="Logo" />
      <Header as="h2" style={{ color: "#31E981" }}>
        Welcome to the game {room.id}
      </Header>
      <div className="row">
        <CharacterList room={props.room} me={props.user} leaveRoomHandler={leaveRoom} />
        <SettingsCheckBox />
      </div>
      <Button>Let's Play</Button>
    </Container>
  );
};

const mapStateToProps = (store: IAppState) => {
  return {
    user: store.userState.user,
    room: store.roomState.room,
    roomSettings: store.userState.roomSettings,
  };
};

const mapDispatchToProps = {
  leaveRoom: (user: IUser) => userAction.LeaveRoom(user),
  // setRoomInGame: (room: IRoom) => roomAction.SetRoomInGame(room)
};

export default connect(mapStateToProps, mapDispatchToProps)(GamePage);
