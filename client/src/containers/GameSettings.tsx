import React from "react";
import { connect } from "react-redux";
import { cssRaw } from "typestyle";
import { Grid, Header, Button } from "semantic-ui-react";

import { IAppState } from "../store/Store";
import { IRoom, IGameSettings } from "../reducers/roomReducer";
import { IUser } from "../reducers/userReducers";
import * as RoomAction from "../actions/roomActions";
import GameModeCheckBox from "../components/GameModeCheckBox";
import ModeCheckBox from "../components/ModeCheckBox";
import Tetrominos from "src/containers/tetrominos.png";

cssRaw(`
  .Tetrominos {
    height: 20vmin;
    margin: 20px;
    pointer-events: none;
  }

  @media (prefers-reduced-motion: no-preference) {
    .Tetrominos {
      animation: Tetrominos-spin infinite 20s linear;
    }
  }

  @keyframes Tetrominos-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(1000deg);
    }
  }
`);

interface IProps {
  user: IUser;
  room: IRoom;
  roomError: string;
  gameSettings: IGameSettings;
  refreshGameSettings: ({ room: IRoom, gameSettings: IGameSettings }: RoomAction.SettingsGameTypeNested) => void;
  gameSettingsDone: (done: boolean) => void;
}

const GameSettings: React.FC<IProps> = props => {
  const { refreshGameSettings } = props;

  const submitted = (e: React.MouseEvent) => {
    e.preventDefault();
    props.gameSettingsDone(true);
  };

  return (
    <div>
      <Header as="h1" textAlign="center">
        Prepare Your Setup
      </Header>
      <img src={Tetrominos} className={"Tetrominos"} alt="Logo" />
      <Grid textAlign="center">
        <Grid.Column>
          <ModeCheckBox room={props.room} callBackHandler={refreshGameSettings} gameSettings={props.gameSettings} />
          <GameModeCheckBox
            room={props.room}
            gameSettings={props.gameSettings}
            refreshGameSettings={refreshGameSettings}
          />
          <br />
          <Button onClick={submitted} name="gameSettingsDone">
            Let's Play
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

const mapStateToProps = (store: IAppState) => {
  return {
    room: store.roomState.room,
    user: store.userState.user,
    roomError: store.roomState.error,
    gameSettings: store.roomState.gameSettings,
  };
};

const mapDispatchToProps = {
  refreshGameSettings:
    ({room, gameSettings}: RoomAction.SettingsGameTypeNested) => RoomAction.RefreshGameSettings({room, gameSettings}),
  gameSettingsDone: (done: boolean) => RoomAction.GameSettingsDone(done)
};

export default connect(mapStateToProps, mapDispatchToProps)(GameSettings);
