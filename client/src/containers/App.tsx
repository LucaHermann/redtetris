import React from "react";
import { Switch, Route } from "react-router";
import { cssRaw } from "typestyle";
import { ToastProvider } from "react-toast-notifications";

import HomePage from "./Home";
import FinalRoomPage from "./FinalRoom";

cssRaw(`
  .App {
    background-color: #00120B;
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    justify-content: center;
    font-size: calc(10px + 2vmin);
    min-height: 100vh;
    color: white !important;
  }
  `);

const App: React.FC<any> = () => {
  return (
    <ToastProvider>
      <div className="App">
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/:room[:userId]" exact component={FinalRoomPage} />
        </Switch>
      </div>
    </ToastProvider>
  );
};

export default App;
